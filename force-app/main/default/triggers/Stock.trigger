trigger Stock on Stock__c(
    before insert,
    before update,
    after insert,
    after update
) {
    if (Trigger.isAfter && Trigger.isUpdate) {
        RentalGenerator generator = new RentalGenerator();
        generator.generateRentalsForRentedStock(Trigger.oldMap, Trigger.new);
        // Trigger.old = List
        // Trigger.new = Map
    }
}
