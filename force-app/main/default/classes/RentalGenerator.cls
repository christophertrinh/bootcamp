public with sharing class RentalGenerator {
    public void generateRentalsForRentedStock(
        Map<Id, Stock__c> oldStockMap,
        List<Stock__c> updatedStock
    ) {
        List<Rental__c> newRentals = new List<Rental__c>();

        for (Stock__c stock : updatedStock) {
            if (
                stock.Status__c == 'Rented' &&
                oldStockMap.get(stock.Id).Status__c != 'Rented'
            ) {
                newRentals.add(
                    new Rental__c(
                        Stock__c = stock.Id,
                        Customer__c = UserInfo.getUserId()
                    )
                );
            }
        }
        if (newRentals.size() > 0) {
            insert newRentals;
        }
    }
}
