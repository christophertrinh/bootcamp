@isTest
public with sharing class RentalGeneratorTest {
    @isTest
    public static void generateRentalsForRentedStock_givenNewlyRentedStock_shouldGenerateRental() {
        Title__c testTitle = generateTitle('Test title', 'Comedy');
        insert testTitle;

        Stock__c testStock = generateStock(testTitle.Id);
        insert testStock;

        RentalGenerator generator = new RentalGenerator();
        Test.startTest();
        testStock.Status__c = 'Rented';
        update testStock;
        Test.stopTest();

        List<Rental__c> createdRentals = [
            SELECT Customer__c, Stock__c
            FROM Rental__c
        ];

        System.debug(createdRentals);

        System.assertEquals(
            1,
            createdRentals.size(),
            'Expected a single rental'
        );
        System.assertEquals(
            testStock.Id,
            createdRentals[0].Stock__c,
            'The stock should have been assigned properly'
        );
        System.assertEquals(
            UserInfo.getUserId(),
            createdREntals[0].Customer__c,
            'The customer should be the current user'
        );
    }

    public static void generateRentalsForRentedStock_givenNewlyRentedStock_shouldNotGenerateRental() {
        Title__c testTitle = generateTitle('Test title', 'Comedy');
        insert testTitle;

        Stock__c testStock = generateStock(testTitle.Id);
        insert testStock;

        RentalGenerator generator = new RentalGenerator();
        Test.startTest();
        testStock.Status__c = 'Damaged';
        update testStock;
        Test.stopTest();

        List<Rental__c> createdRentals = [
            SELECT Customer__c, Stock__c
            FROM Rental__c
        ];

        System.assertEquals(
            0,
            createdRentals.size(),
            'Expected no rentals to be generated'
        );
    }

    private static Title__c generateTitle(String name, String genre) {
        Title__c title = new Title__c();
        title.Name = name;
        title.Genre__c = genre;

        return title;
    }

    private static Stock__c generateStock(Id titleId) {
        Stock__c stock = new Stock__c();
        stock.Title__c = titleId;
        stock.Status__c = 'Available';

        return stock;
    }
}
