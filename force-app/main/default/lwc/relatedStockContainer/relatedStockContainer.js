import { LightningElement, wire, track } from "lwc";
import { subscribe, MessageContext } from "lightning/messageService";
import TITLE_SELECTED_MC from "@salesforce/messageChannel/Title_Selected__c";
import getAllChildStock from "@salesforce/apex/StockAuraService.getAllChildStock";

export default class RelatedStockContainer extends LightningElement {
    @wire(MessageContext) messageContext;
    @track stocks;
    @track error;
    @track selectedStockId;
    @track title = "";

    connectedCallback() {
        //On initial load - Run this method
        this.subscribeToTitleSelected();
    }

    subscribeToTitleSelected() {
        subscribe(this.messageContext, TITLE_SELECTED_MC, (message) =>
            this.handleTitleSelected(message)
        );
    }

    handleTitleSelected(message) {
        this.title = message.title;
        getAllChildStock({ titleId: message.title.Id })
            .then((stocks) => {
                //console.log('stocks: ' + JSON.stringify(stocks));
                this.stocks = stocks;
            })
            .catch((error) => {
                this.error = error;
            });
    }

    handleStockClicked(event) {
        console.log("stock id: ", event.detail);
        this.selectedStockId = event.detail;
    }
}
