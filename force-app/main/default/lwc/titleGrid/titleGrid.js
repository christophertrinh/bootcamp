import { api, LightningElement } from "lwc";

export default class TitleGrid extends LightningElement {
    @api titles;

    handleTitleClick(e) {
        alert("Hello from " + e.detail.titleName); // eslint-disable-line no-alert
    }
}
