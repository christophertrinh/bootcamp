import { api, LightningElement, wire } from "lwc";
import { publish, MessageContext } from "lightning/messageService";
import TITLE_SELECTED_MC from "@salesforce/messageChannel/Title_Selected__c";

export default class TitleCard extends LightningElement {
    @api title;
    @wire(MessageContext) messageContext;

    get availableStockString() {
        return `Available Stock: ${this.title.Available_Stock__c}`;
    }

    handleClick() {
        const payload = { title: this.title };
        publish(this.messageContext, TITLE_SELECTED_MC, payload);
        // How to dispatch an event from within components of the same hierarchy
        // this.dispatchEvent(
        //     new CustomEvent("titleclicked", {
        //         detail: { titleName: this.title.Name }
        //     })
        // );
    }
}
