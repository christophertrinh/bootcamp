import { api, LightningElement } from "lwc";
import getLatestTitles from "@salesforce/apex/TitleAuraService.getLatestTitles";

export default class HomeTitleContainer extends LightningElement {
    @api limiter;
    titles;

    connectedCallback() {
        getLatestTitles({ limiter: this.limiter })
            .then((response) => {
                console.log(response);
                this.titles = response;
            })
            .catch((error) => {
                console.error(error);
            });
    }
}
